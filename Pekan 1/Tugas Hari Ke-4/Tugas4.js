//Soal 1
console.log("SOAL 1")
console.log("LOOPING PERTAMA")
var pertama = 2;
while(pertama <= 20 ) {
    console.log(pertama + ' - I love coding');
    pertama+= 2;
}

console.log("LOOPING KEDUA")
var kedua = 20;
while(kedua > 0) {
    console.log(kedua + ' - I will become a frontend developer');
    kedua-= 2; 
}
console.log()

//Soal 2
console.log("SOAL 2")
for (var deret = 1; deret <= 20; deret++) {
   if (deret % 3 === 0 && deret % 2 === 1 ){    
       console.log(deret + ' - I love coding');
    }
    else if (deret % 2 === 0) {
       console.log(deret + ' - Berkualitas');
    }
    else if (deret % 2 === 1) {
        console.log(deret + ' - Santai');
    }
}
console.log()

//Soal 3
console.log("SOAL 3")
var i, j, p='';
for(i=1; i<=7; i++) {
    for(j=1; j<=i; j++){
        p += '#';
    }
   p+= '\n' 
}
console.log(p)

//Soal 4
console.log("SOAL 4")
var kalimat="saya sangat senang belajar javascript"
var pisah = kalimat.split(" ")
console.log(pisah)
console.log()

//Soal 5
console.log("SOAL 5")
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()
for (var i = 0; i<= 4; i++){
    console.log(daftarBuah[i])
}
