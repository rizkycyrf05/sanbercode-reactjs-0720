//Soal 2
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
readBooksPromise(10000,books[0])
    .then((fulfilled) => {
        readBooksPromise(fulfilled, books[1])
            .then((fulfilled) => {
                readBooksPromise(fulfilled, books[2])
            })    
            .catch((error) => {
                (error.message);
            })   
    })
    .catch((error) => {
        (error.message);
    });   
        

