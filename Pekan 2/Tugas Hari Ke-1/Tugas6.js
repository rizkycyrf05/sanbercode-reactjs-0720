//Soal 1
//ubahlah array di bawah ini menjadi object dengan property nama, jenis kelamin, hobi dan tahun lahir
//var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
console.log("Soal 1");
var objectDaftarPeserta = {
    nama : "Asep",
    jenisKelamin : "Laki-Laki",
    hobi : "Reading Book",
    tahunLahir : 1992
}
console.log(objectDaftarPeserta);
console.log();

//Soal 2
//anda diberikan data-data buah seperti di bawah ini
/*1.nama: strawberry
  warna: merah
  ada bijinya: tidak
  harga: 9000 
2.nama: jeruk
  warna: oranye
  ada bijinya: ada
  harga: 8000
3.nama: Semangka
  warna: Hijau & Merah
  ada bijinya: ada
  harga: 10000
4.nama: Pisang
  warna: Kuning
  ada bijinya: tidak
  harga: 5000 */
//uraikan data tersebut menjadi array of object dan munculkan data pertama
console.log("Soal 2");
var buah = [{nama: "strawberry", warna: "merah", ada_bijinya: "tidak", harga: 9000}, 
            {nama: "jeruk", warna: "oranye", ada_bijinya: "ada", harga: 8000},
            {nama: "Semangka", warna: "Hijau & Merah", ada_bijinya: "ada", harga: 10000},
            {nama: "Pisang", warna: "kuning", ada_bijinya: "tidak", harga: 5000}
]

var buahFilter = buah.filter(function(item){
    return item.warna != "kuning" && item.ada_bijinya != "ada" ;
})
console.log(buahFilter);

//Soal 3
//buatlah variable seperti di bawah ini
//var dataFilm = []
//buatlah fungsi untuk menambahkan dataFilm tersebut yang itemnya object adalah object dengan property nama, durasi , genre, tahun
console.log("Soal 3");
var dataFilm = []
var tambahData = {
    nama : "Sonic The Hedgedog",
    durasi : "1 Jam 38 Menit",
    genre : "Family & Fantasy",
    tahun : 2020  
}
function tambahkanData(tambahData){
    dataFilm.push(tambahData);
}
tambahkanData(tambahData)
console.log(dataFilm);
console.log();

//Soal 4
console.log("Soal 4");
console.log("Release 0")
//Release 0
class Animal {
    constructor(name){
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
    get anam(){
        return this.name;
    }
    
}
var sheep = new Animal("shaun",);
 
console.log(sheep.anam);
console.log(sheep.legs);
console.log(sheep.cold_blooded);
//Release 1
console.log("Release 1");
class Ape extends Animal {
    constructor(name) {
        super(name);
        this.legs = 2;
    }
    yell(){
        return "Auooo";
    }
}
class Frog extends Animal {
    constructor(name) {
        super(name);
    }
    jump() {
        return "hop hop";
    }
}
var sungokong = new Ape("kera sakti")
console.log(sungokong.name);
console.log(sungokong.legs);
console.log(sungokong.yell())
 
var kodok = new Frog("buduk")
console.log(kodok.name);
console.log(kodok.legs);
console.log(kodok.jump())

console.log()

//Soal 5
console.log("Soal 5")
class Clock {
    constructor({template}) {
        this.template = template;
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output); 
    }

    stop() {
        clearInterval(timer);
    };

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    };

}
var clock = new Clock({template: 'h:m:s'});
clock.start();  

