// Javascript ES6 – Part 1
//Soal 1
//buatlah fungsi menggunakan arrow function luas lingkaran dan keliling lingkaran dengan arrow function 
//lalu gunakan let dan const di dalam soal ini
console.log('Soal 1');
let r = 6;
const phi = 3.14;

let luasLingkaran = () =>{
    return phi * Math.pow(r, 2);
}
let kelilingLingkaran = () =>{
    return 2 * phi * r;
}
console.log('Luas Lingkarang = ' + luasLingkaran());
console.log('Keliling Lingkarang = ' + kelilingLingkaran());
console.log();

//Soal 2
//buatlah variable seperti di bawah ini:
/*let kalimat = ""
buatlah fungsi menambahkan kata di kalimat dan gunakan penambahan kata tersebut dengan template literal, 
berikut ini kata kata yang mesti di tambahkan
saya
adalah
seorang
frontend
developer*/
console.log('Soal 2');
let k1 = "saya";
let k2 = "adalah";
let k3 = "seorang";
let k4 = "frontend";
let k5 = "developer";
let kalimat = `${k1} ${k2} ${k3} ${k4} ${k5}.`;
console.log(kalimat);
console.log();

//Soal 3
//buatlah class Book yang didalamnya terdapat property name, totalPage, price. 
//lalu buat class baru komik yang extends terhadap buku dan mempunyai property sendiri yaitu isColorful yang isinya true atau false
console.log('Soal 3');
class Book {
    constructor(name, totalPage, price){
        this.name = name;
        this.totalPage = totalPage;
        this.price = price;
    }
}

class Komik extends Book {
    constructor(name, totalPage, price, isColorful){
        super(name, totalPage, price);
        this.isColorful = isColorful
    }
}
 myBook = new Book('Javascript', 300, 150000);
 myKomik = new Komik('Sonic', 60, 40000, true);
console.log(myBook);
console.log(myKomik);
